<?php

/**
 * Plugin Name: Giant Peach Campaign Monitor
 * Description: Adds functionality for Campaign Monitor signup
 * Version: 0.1.4
 * Author: Giant Peach
 * Author URI: https://giantpeach.agency
 * Text Domain: gp_campaignmonitor
 *
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

add_action('init', 'gp_campaignmonitor_post_check');

function gp_campaignmonitor_post_check()
{

    $name = '';

    if (!isset($_POST['cpmonitoremail'])) {
        return;
    }

    if (isset($_POST['cpmonitorname'])) {
        $name = $_POST['cpmonitorname'];
        $name = filter_var(trim($name), FILTER_SANITIZE_STRING);
    }

    $email = $_POST['cpmonitoremail'];

    $response = array(
        'success' => false,
        'errors' => array()
    );

    if (env('RECAPTCHA_SECRET') && $_POST['RecaptchaResponse']) {
        if (!gp_campaignmonitor_recaptcha($_POST['RecaptchaResponse'])) {
            header("HTTP/1.1 500 Internal Server Error");
            header('Content-type: application/json; charset=utf-8');
            echo json_encode([]);
            exit;
        }
    }

    $response = gp_campaignmonitor_validate_email($email, $response);
    $email = isset($response['email']) ? $response['email'] : null;
    $response = $response['response'];

    // Missing and/or invalid?
    if (!isset($response['missing']) && !isset($response['invalid'])) {
        $response = gp_campaignmonitor_signup($email, $name, array(), $response);
    }

    header("HTTP/1.1 200 OK");
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($response);
    exit;
}

function gp_campaignmonitor_recaptcha($token)
{
    $recaptcha = new \ReCaptcha\ReCaptcha(env('RECAPTCHA_SECRET'));

    $resp = $recaptcha->setScoreThreshold(0.5)->verify($token, $_SERVER['REMOTE_ADDR']);

    if ($resp->isSuccess()) {
        return true;
    }

    return false;
}

add_action('email_newsletter_signup', 'gp_campaignmonitor_signup_check', 10, 4);

function gp_campaignmonitor_signup_check($email, $name = '', $customFields = array(), $show_notice = false)
{

    $response = array(
        'success' => false,
        'errors' => array()
    );

    $response = gp_campaignmonitor_validate_email($email, $response);
    $email = isset($response['email']) ? $response['email'] : null;
    $response = $response['response'];

    // Missing and/or invalid?
    if (!isset($response['missing']) && !isset($response['invalid'])) {
        $response = gp_campaignmonitor_signup($email, $name, $customFields, $response);
    }

    if ($show_notice && function_exists('wc_add_notice')) {
        if ($response['success'] === true) {
            wc_add_notice('Thank you for signing up to our newsletter!', 'success');
        } else {
            $message = 'An error occured signing you up to our newsletter.';
            foreach ($response['errors'] as $error) {
                $message .= ' ' . $error;
            }
            wc_add_notice($message, 'error');
        }
    }

    return $response;
}

add_action('email_multiple_newsletter_signup', 'gp_campaignmonitor_multiple_signup_check', 10, 5);

function gp_campaignmonitor_multiple_signup_check($email, $name, $customFields = [], $show_notice = false, $lists = [])
{
    $response = array(
        'success' => false,
        'errors' => array()
    );

    $response = gp_campaignmonitor_validate_email($email, $response);
    $email = isset($response['email']) ? $response['email'] : null;
    $response = $response['response'];

    // Missing and/or invalid?
    if (!isset($response['missing']) && !isset($response['invalid']) && count($lists) > 0) {
        foreach ($lists as $list) {
            $response = gp_campaignmonitor_signup($email, $name, $customFields, $response, $list);
        }
    }

    if ($show_notice && function_exists('wc_add_notice')) {
        if ($response['success'] === true) {
            wc_add_notice('Thank you for signing up to our newsletter!', 'success');
        } else {
            $message = 'An error occured signing you up to our newsletter.';
            foreach ($response['errors'] as $error) {
                $message .= ' ' . $error;
            }
            wc_add_notice($message, 'error');
        }
    }

    return $response;
}

function gp_campaignmonitor_validate_email($email, $response)
{

    $missing = array();
    $invalid = array();

    $sanitisedEmail = null;

    if (empty(trim($email))) {
        $missing['email'] = 'Email';
    } else {
        $sanitisedEmail = filter_var(trim($email), FILTER_SANITIZE_EMAIL);

        // Validate the sanitised Email
        if (!empty($sanitisedEmail)) {
            if (!filter_var($sanitisedEmail, FILTER_VALIDATE_EMAIL)) {
                $invalid['email'] = 'Email address';
            }
        }
    }

    // Missing?
    if (!empty($missing)) {
        $response['missing'] = $missing;
        $plural = ($missing > 1) ? 's are' : ' is';
        // $response['errors'][] = '<p>The following required field'.$plural.' missing:</p><ul><li>'.implode('</li><li>', $missing).'</li></ul>';
        $response['errors'][] = 'Invalid email address.';
    }

    // Invalid?
    if (!empty($invalid)) {
        $response['invalid'] = $invalid;
        $plural = ($invalid > 1) ? 's are' : ' is';
        // $response['errors'][] = '<p>The following required field'.$plural.' not in the correct format:</p><ul><li>'.implode('</li><li>', $invalid).'</li></ul>';
        $response['errors'][] = 'Invalid email address.';
    }

    return array('email' => $sanitisedEmail, 'response' => $response);
}

function gp_campaignmonitor_signup($email, $name = '', $customFields = array(), $response, $listID = null)
{

    $jiffyMailAuth = array(
        'api_key' => env('JIFFYMAIL_API_KEY')
    );

    $signupListID = $listID ? $listID : env('JIFFYMAIL_LIST_ID');

    $jiffyMailWrap = new CS_REST_Subscribers($signupListID, $jiffyMailAuth);

    $result = $jiffyMailWrap->add(
        array(
            'EmailAddress' => $email,
            'Name' => $name,
            'CustomFields' => $customFields,
            'ConsentToTrack' => "unchanged",
            'Resubscribe' => true,
            'RestartSubscriptionBasedAutoResponders' => true
        )
    );

    $response['result'] = $result;

    if (is_object($result)) {
        if (!empty($result->http_status_code) && $result->http_status_code == 201) {
            $response['success'] = true;
        } else {
            if (!empty($result->response->Message)) {
                $response['errors'][] = $result->response->Message;
            }
        }
    }

    return $response;
}

function gp_campaignmonitor_get_lists()
{
    $jiffyMailAuth = array(
        'api_key' => env('JIFFYMAIL_API_KEY')
    );

    $jiffyMailClientId = env('JIFFYMAIL_CLIENT_ID');

    $jiffyMailWrap = new CS_REST_Clients($jiffyMailClientId, $jiffyMailAuth);

    $lists = $jiffyMailWrap->get_lists();

    if ($lists->http_status_code === 401) {
        return false;
    }

    return $lists->response;
}
