# Giant Peach Campaign Monitor Plugin

## How to use

* include JIFFYMAIL_API_KEY in .env
* include JIFFYMAIL_LIST_ID in .env (this can be found by viewing List in Jiffymail and clicking "change name/type" link)
* (OPTIONAL) include JIFFYMAIL_CLIENT_ID in .env

### AJAX sign up

* POST `'cpmonitoremail'` value to index page (or any page) via AJAX to trigger sign up and receive JSON response.

### Signup via action

* Call `do_action('email_newsletter_signup', $email, $show_notice);` anywhere to hook in to Campaign Monitor signup.

### Signing up to Multiple Lists

In some instances you may wish to display the available lists so a user can choose what to sign up for. In order to pull the List information from the API, add JIFFYMAIL_CLIENT_ID to your .env. You can then call gp_campaignmonitor_get_lists() in your template to return an array of CS_REST_Wrapper_Result objects with ListID and Name.

You can loop over these and create your checkboxes e.g.:

`<input name="newsletter[]" value="<?php echo $list->ListID; ?>" id="newsletter-<?php echo $list->ListID; ?>" type="checkbox" class="checkbox--alt" />`

There is a new quick and dirty action to handle signing up to multiple lists, you can use it like so:

`
$newsletterArr = explode(", ", $formData['newsletter']);`

`
        do_action('email_multiple_newsletter_signup', $formData['email'], $name, array(), false, $newsletterArr);
`